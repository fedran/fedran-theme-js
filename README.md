# Digital and Print Theme for Fedran

This is the digital and print theme for books generated in the Fedran universe. It follows the standard conventions used in the earlier books to format epigraphs (as block quotes) and chapter entries.

## Fonts

The primary fonts in this theme are:

-   Source Serif Pro
-   Source Sans Pro (for IPA)
