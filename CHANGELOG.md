## [4.0.2](https://gitlab.com/fedran/fedran-theme-js/compare/v4.0.1...v4.0.2) (2022-07-21)


### Bug Fixes

* correcting handling of epigraphs and first paragraph ([b218018](https://gitlab.com/fedran/fedran-theme-js/commit/b2180183f420f6838f65200ea8811703a276e473))

## [4.0.1](https://gitlab.com/fedran/fedran-theme-js/compare/v4.0.0...v4.0.1) (2022-01-10)


### Bug Fixes

* correcting template liquid handling ([328e1cb](https://gitlab.com/fedran/fedran-theme-js/commit/328e1cbd29f2ea68c9e794d741acf1f772a3fb75))

# [4.0.0](https://gitlab.com/fedran/fedran-theme-js/compare/v3.0.5...v4.0.0) (2022-01-10)

## [3.0.5](https://gitlab.com/fedran/fedran-theme-js/compare/v3.0.4...v3.0.5) (2020-01-02)


### Bug Fixes

* tweaking title page generation ([f25c76f](https://gitlab.com/fedran/fedran-theme-js/commit/f25c76fffa677c44cd7ed6e7acf0f98d026305d8))

## [3.0.4](https://gitlab.com/fedran/fedran-theme-js/compare/v3.0.3...v3.0.4) (2020-01-02)


### Bug Fixes

* packaging fonts again ([531c7ee](https://gitlab.com/fedran/fedran-theme-js/commit/531c7ee5bcdabdd65f7c9734fd9ee2505a1e4538))

## [3.0.3](https://gitlab.com/fedran/fedran-theme-js/compare/v3.0.2...v3.0.3) (2020-01-02)


### Bug Fixes

* tweaking font for title page ([ae93d41](https://gitlab.com/fedran/fedran-theme-js/commit/ae93d417e12e0b1d1f614c340f755fbd0097848e))
* tweaking title page generation ([465db91](https://gitlab.com/fedran/fedran-theme-js/commit/465db919f6f053e1b1cf24d0c5c302789b042c44))

## [3.0.1](https://gitlab.com/fedran/fedran-theme-js/compare/v3.0.0...v3.0.1) (2020-01-02)


### Bug Fixes

* package the OTF fonts directly ([52e0b71](https://gitlab.com/fedran/fedran-theme-js/commit/52e0b71aac2dd22e77a832533a238f837a5f5603))

# [2.1.0](https://gitlab.com/fedran/fedran-theme-js/compare/v2.0.2...v2.1.0) (2020-01-02)


### Features

* changed base font from Corda to Source Serif Pro ([f08bb16](https://gitlab.com/fedran/fedran-theme-js/commit/f08bb160a63d0a9f7f693e2b1e4ea73dbea14a36))

## [2.0.2](https://gitlab.com/fedran/fedran-theme-js/compare/v2.0.1...v2.0.2) (2019-04-15)


### Bug Fixes

* switching to ES6 to handle module loading ([d18d2cf](https://gitlab.com/fedran/fedran-theme-js/commit/d18d2cf))

## [2.0.1](https://gitlab.com/fedran/fedran-theme-js/compare/v2.0.0...v2.0.1) (2019-04-15)


### Bug Fixes

* return the theme the way [@fedran](https://gitlab.com/fedran)/build expects it ([59e5bef](https://gitlab.com/fedran/fedran-theme-js/commit/59e5bef))

# [2.0.0](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.5...v2.0.0) (2019-04-15)


### Features

* **fonts:** added encrypted source fonts for consistent building ([8d294ad](https://gitlab.com/fedran/fedran-theme-js/commit/8d294ad))


### BREAKING CHANGES

* **fonts:** Fonts are no longer based on the environment and any CI jobs need to be modified to
handle.

## [1.2.5](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.4...v1.2.5) (2018-08-26)


### Bug Fixes

* fixing the margin so it matches previous PDF version ([f402bfd](https://gitlab.com/fedran/fedran-theme-js/commit/f402bfd))

## [1.2.4](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.3...v1.2.4) (2018-08-26)


### Bug Fixes

* removed spacing on the first page ([f301e17](https://gitlab.com/fedran/fedran-theme-js/commit/f301e17))

## [1.2.3](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.2...v1.2.3) (2018-08-12)


### Bug Fixes

* moving the word breaking into the em tags ([e1b1ff5](https://gitlab.com/fedran/fedran-theme-js/commit/e1b1ff5))
* removing word breaking to use default rules ([bdc7ca8](https://gitlab.com/fedran/fedran-theme-js/commit/bdc7ca8))

## [1.2.2](https://gitlab.com/fedran/fedran-theme-js/compare/v1.2.1...v1.2.2) (2018-08-12)


### Bug Fixes

* added package management ([112c1ff](https://gitlab.com/fedran/fedran-theme-js/commit/112c1ff))
