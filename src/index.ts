import { FedranTheme } from "./FedranTheme";

function loadFedranTheme() {
    return new FedranTheme();
}

export default loadFedranTheme;
